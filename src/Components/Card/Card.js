import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./Card.css"
export class Card extends Component {
    constructor(props){
        super(props)
        this.state={

        }
    }

  render() {
    return (
      
      <Link to={`/products/${this.props.id}`} className='col-lg-3 col-md-4 Card my-custom-link'>
        <h3>{this.props.title}</h3>
        <img  className="image" src={this.props.productImage} alt={this.props.title}/>
        <p className='description'>{this.props.description}</p>
        <p>{this.props.rating.rate} out of 5</p>
        <button className='btn btn-dark price'>${this.props.price}</button>
      </Link>
    
    )
  }
}

export default Card