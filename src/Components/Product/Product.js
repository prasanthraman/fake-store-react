import React, { Component } from 'react'
import axios from 'axios'
import Loader from '../Loader/Loader'
import "./Product.css"

export default class Product extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null,
            dataFetched: false,
            apiFailed: false,
            doesNotexist: false,
        }

    }

    componentDidMount() {
        this.fetchdata()
    }
    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            this.setState({
                data: null,
                dataFetched: false,
                apiFailed: false,
            })
            this.fetchdata()
        }
    }
    fetchdata = () => {
        if (this.props.match.params.id < 1 || this.props.match.params.id > 20 || isNaN(this.props.match.params.id)) {
            this.setState({
                doesNotexist: true,
            })

        } else {

            axios.get(`https://fakestoreapi.com/products/${this.props.match.params.id}`)
                .then((response) => {
                    console.log(response.data)
                    this.setState({
                        data: response.data,
                        dataFetched: true
                    })
                })
                .catch((err) => {
                    this.setState({
                        apiFailed: true
                    })
                })
        }
    }

    render() {
        return (
            <div>
                {this.state.doesNotexist ? <h2 className='text-danger text-center'>Sorry page does not exist!</h2> :
                    this.state.dataFetched ?
                        <div className='Card Product'>
                            <h3>{this.state.data.title}</h3>
                            <img className="image" src={this.state.data.image} alt={this.state.data.title} />
                            <p className='description'>{this.state.data.description}</p>
                            <p>{this.state.data.rating.rate} out of 5</p>
                            <button className='btn btn-success price'
                                onClick={() => {
                                   
                                    this.props.addItemToCart(this.state.data)
                                }}>
                                Add item to Cart - ${this.state.data.price}
                            </button>
                        </div>
                        :
                        this.state.apiFailed ?
                            "Sorry Failed to fetch data!"
                            :
                            <Loader />
                }
            </div>
        )
    }

}
