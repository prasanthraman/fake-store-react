import React, { Component } from 'react'
import axios from 'axios'
import Loader from '../Loader/Loader'
export default class CartElement extends Component {
    constructor(props) {
        super(props)

        this.state = {

            dataFetched: false,
            apiFailed: false,
            products: [],
            totalValue: 0,

        }

    }
    componentDidMount() {

        this.props.data.products.map((item) => {

            this.fetchProduct(item.productId)

            return item.id
        })

    }

    fetchProduct = (id) => {

        axios.get(`https://fakestoreapi.com/products/${id}`)
            .then((response) => {
                this.setState((prevState) => {

                    return {
                        dataFetched: true,
                        products: [...prevState.products, response.data],
                    }

                })

            })
            .catch((err) => {
                this.setState({
                    dataFetched: false,
                    apiFailed: true,
                })
            })

    }

    findProduct = (id) => {


        let cartItem = this.state.products.find((item) => {

            if (item.id == id) {
                return true
            } else {
                return false
            }
        })
        return cartItem


    }




    render() {
        let date = new Date(this.props.data.date).toLocaleDateString('en-US')
        let total = 0

        return (this.state.dataFetched ?
            <div className='container card mb-5'>
                <div className='row'>
                    <p className='col-6'>Order Id: <strong>{this.props.data.id}</strong></p>
                    <p className='col-6 text-end'>User Id: <strong> {this.props.data.userId}</strong> </p>
                    <p className='col-6'>Date: <strong>{date}</strong> </p>
                    <div className='col-12 d-flex justify-content-center align-items-center flex-column'>{this.props.data.products.map((item, index) => {

                        let product = this.findProduct(item.productId)

                        if (typeof (product) == 'undefined') {
                            product = {}
                        } else {
                            if (!isNaN(product.price)) {
                                total = total + (product.price * item.quantity)
                            }

                        }

                        return <div className='card p-1 d-flex m-2 justify-content-center align-items-center col-8 bg-light'>
                            <h5> {product.title}</h5>
                            <img src={product.image} alt={product.title} width="100" height="100" />
                            <p className='text-success'>Quantity: {item.quantity}</p>
                            <p className='text-primary '><h4 className=' d-inline '> ${product.price * item.quantity}</h4></p>
                        </div>
                    })}
                    </div>
                    <h3 className='text-end'>= ${total}</h3>

                </div>
            </div>
            : this.apiFailed ? "Sorry failed to fetch data" :
                <Loader />
        )
    }
}
