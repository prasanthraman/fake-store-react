import React, { Component } from 'react'

import { NavLink } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './Navbar.css'
export class Navbar extends Component {
    render() {
        return (
            <div>
                <nav className='navbar navbar-expand-md navbar-dark bg-dark Navbar'>
                    <NavLink to='/' replace className='navbar-brand'> My E-Store</NavLink>
                    <ul className='navbar-nav'>
                        <li className='nav-item'>
                            <p className='nav-link'>Login</p>
                        </li>
                        <li className='nav-item'>
                            <p className='nav-link'>Sign Up</p>
                        </li>
                        <li className='nav-item'>
                            <NavLink to='/cart' className='nav-link'>Cart</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Navbar