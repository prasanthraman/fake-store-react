import axios from 'axios'
import React, { Component } from 'react'
import CartElement from '../CartElement/CartElement'
import Loader from '../Loader/Loader'

export default class Cart extends Component {
    constructor(props) {
        super(props)

        this.state = {

            data: null,
            dataFetched: false,
            apiFailed: false,

        }


    }
    componentDidMount() {

        this.fetchCart()

    }




    fetchCart = () => {
        axios.get('https://fakestoreapi.com/carts')
            .then((response) => {
                this.setState({
                    data: response.data,
                    dataFetched: true,

                })
            })
            .catch((err) => {
                this.setState({
                    dataFetched: true,
                    apiFailed: true,
                })
            })
    }




    render() {

        return (
            <div>
                <h2 className='text-center text-success'>Cart</h2>
                {

                    this.state.dataFetched ?
                        <div>{this.state.data.map((cart) => {
                            return <CartElement
                                key={cart.id}
                                data={cart}
                            />
                        })}</div>
                        : this.state.apiFailed ?
                            "Sorry there was an error fetching data!"
                            : <Loader />
                }


            </div>

        )
    }
}
