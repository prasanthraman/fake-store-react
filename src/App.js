import React from 'react'
import axios from 'axios'

import Loader from './Components/Loader/Loader'
import Card from './Components/Card/Card'

import './App.css'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: null,
      dataFetched: false,
      apiFailed: false,
    }
  }

  componentDidMount() {
    this.fetchdata()
  }

  fetchdata = () => {
    axios.get("https://fakestoreapi.com/products")
      .then((response) => {
        console.log(response.data)
        this.setState({
          data: response.data,
          dataFetched: true
        })
      })
      .catch((err) => {
        this.setState({
          apiFailed: true
        })
      })
  }

  render() {
    return (
      <div className="App">
        <div className='row Container'>

          {this.state.dataFetched ?
            this.state.data.map((product) => {
              return <Card
                key={product.id}
                id={product.id}
                title={product.title}
                productImage={product.image}
                description={product.description}
                price={product.price}
                rating={product.rating}
                category={product.category}
              />

            })
            : this.state.apiFailed ? "Sorry Failed to Fetch data!" :
              <Loader className="loader" />}
        </div>




      </div>
    )
  }
}

export default App
