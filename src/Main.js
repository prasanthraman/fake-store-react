
import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Navbar from './Components/Navbar/Navbar'
import Product from './Components/Product/Product'
import App from './App'
import Cart from './Components/Cart/Cart'


export default class Main extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cartCount: 0,
            items: [],
        }

    }

    addItemToCart = (item) => {
        console.log("hey", item)
        this.setState((prevState) => ({
            items: [...prevState.items, item],
            cartCount: prevState.cartCount + 1,
        }))

    }
    render() {

        return (
            <div>
                <BrowserRouter>
                    <Navbar cartCount={this.state.cartCount} />
                    <Switch>
                        <Route exact path="/" component={App} />
                        <Route exact
                            path="/products/:id"
                            render={(match) => {

                                return <Product {...match} addItemToCart={this.addItemToCart} />
                            }}
                        />
                        <Route path="/cart" render={() => {
                                return <Cart items={this.state.items}/>
                        }} />
                        <Route path="/*" >
                            <h2 className='text-danger text-center'>Sorry page does not exist!</h2>
                        </Route>
                    </Switch>
                </BrowserRouter>
            </div>
        )

    }
}

